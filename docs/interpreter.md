<div align="center">
  <h1>Interpreter</h1>
</div>

<div align="center">
  <img src="interpreter_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Interpreter is a behavioral pattern to define a language grammar and interpreter so that
sentences can be interpreted.**

### Real-World Analogy

_A translator._

Representatives (clients) speak in their native languages, and the translator (interpreter pattern) translates the
speeches, conveying the intended message accurately between speakers (contexts).

### Participants

- :bust_in_silhouette: **Interface**
    - Provides an interface to:
        - `method` object (aka `other_method`)
    - Optionally provides an interface to:
        - `method` object

- :man: **ConcreteObject**
    - ...

• AbstractExpression (RegularExpression)- declares an abstract Interpret operation that is common to all nodes in the
abstract syntax tree.246 BEHAVIORAL PATTERNS CHAPTER 5 • TerminalExpression (LiteralExpression)-implements an Interpret
operation associated with terminal symbols in the grammar.-an instance is required for every terminal symbol in a
sentence. • NonterminalExpression (AlternationExpression, RepetitionExpression, SequenceExpressions)- one such class is
required for every rule R ::= R\R^ • •. Rn in the grammar.- maintains instance variables of type AbstractExpression for
each of the symbols RI through Rn.-implements an Interpret operation for nonterminal symbols in the grammar. Interpret
typically calls itself recursively on the variables representing RI through Jin. • Context- contains information that's
global to the interpreter. • Client-builds (or is given) an abstract syntax tree representing a particular sentence in
the language that the grammar defines. The abstract syntax tree is assembled from instances of the NonterminalExpression
and TerminalExpression classes.-invokes the Interpret operation

### Collaborations

...
Collaborations
• The client builds (oris given) the sentence as an abstract syntax tree ofNonterminalExpression and TerminalExpression
instances. Then the client initializes
the context and invokes the Interpret operation.
• Each NonterminalExpression node defines Interpret in terms of Interpret on
each subexpression. The Interpret operation of each TerminalExpression defines the base case in the recursion.
• The Interpret operations at each node use the context to store and access the
state of the interpreter

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When ... .**

Use the Interpreter pattern when there is a language to interpret, and you can represent statements in the language as
abstract syntax trees. The Interpreter pattern works best when • the grammar is simple. For complex grammars, the class
hierarchy for the grammar becomes large and unmanageable. Tools such as parser generators are a better alternative in
such cases. They can interpret expressions without building abstract syntax trees, which can save space and possibly
time. • efficiency is not a critical concern. The most efficient interpreters are usually not implemented by
interpreting parse trees directly but by first translating them into another form. For example, regular expressions are
often transformed into state machines. But even then, the translator can be implemented by the Interpreter pattern, so
the pattern is still applicable.

### Motivation

- ...

Motivation
If a particular kind of problem occurs often enough, then it might be worthwhile
to express instances of the problem as sentences in a simple language. Then you
can build an interpreter that solves the problem by interpreting these sentences.

For example, searching for strings that match a pattern is a common problem.
Regular expressions are a standard language for specifying patterns of strings.
Rather than building custom algorithms to match each pattern against strings,
search algorithms could interpret a regular expression thatspecifies a set ofstrings
to match.

The Interpreter pattern describes how to define a grammar for simple languages,
represent sentencesin the language, and interpret these sentences.In this example,
the pattern describes how to define a grammarfor regular expressions, represent
a particular regular expression, and how to interpret that regular expression.
Suppose the following grammar defines the regular expressions:

```text
expression ::= literal |alternation |sequence |repetition |
'(' expression ')'
alternation ::- expression '|' expression
sequence ::- expression '&' expression
repetition ::= expression '*'
literal ::- 'a' \ 'b' | 'c' | ... { 'a' | 'b' | 'c' | ... }*
```

The symbol expression is the start symbol, and literal is a terminalsymbol
defining simple words

The Interpreter pattern uses a class to represent each grammar rule. Symbols on
the right-hand side ofthe rule are instance variables ofthese classes.Thegrammar
above is represented by five classes: an abstract class RegularExpressionand its
four subclasses LiteralExpression, AlternationExpression, SequenceExpression,
and RepetitionExpression. The last three classes define variables that hold subexpressions.

Every regular expression defined by this grammar is represented by an abstract
syntax tree made up ofinstances ofthese classes. For example, the abstract syntax
tree

We can create an interpreter for these regular expressions by defining the Interpret
operation on each subclass of RegularExpression.Interpret takes as an argument
the context in which to interpret the expression. The context contains the input
string and information on how much of it has been matched so far. Each subclass
of RegularExpression implements Interpret to match the next part of the input
string based on the current context. For example
• LiteralExpressionwill check ifthe input matches the literal it defines,
• AlternationExpression will check ifthe input matches any ofits alternatives,
• RepetitionExpressionwill check ifthe input has multiple copies of expression
it repeats,
and so on.

### Known Uses

- ...

Programming languages: Compilers and interpreters use this pattern extensively to parse, interpret, and execute code
written in programming languages like Python, JavaScript, etc.

Regular expressions: Interpreters are used to interpret and match patterns in text or strings based on regular
expression syntax.

Query languages: SQL parsers and interpreters use this pattern to interpret queries and perform actions on databases.

Markup languages: Interpreters can be used to process and interpret markup languages like XML, HTML, and Markdown.

Rule-based systems: Systems that use rule-based logic for decision-making or inference engines often implement the
Interpreter pattern to interpret and act upon sets of rules.

Mathematical expressions: Interpreters can be employed to interpret and evaluate mathematical expressions or formulas.

Configuration and scripting languages: Interpreters can be used to process and execute configuration files or scripts in
various systems.

Workflow engines: Interpreters can be used in workflow engines to interpret and execute workflow definitions or scripts
that define a sequence of tasks.

Voice command processing: Systems that interpret and execute voice commands often use the Interpreter pattern to parse
and act upon the spoken language.

Symbolic mathematics: Computer algebra systems use interpreters to manipulate and solve complex mathematical expressions
symbolically.

Game development: Interpreters can be used to process scripts that define behaviors, quests, or events within a game
world.

Network protocols: Interpreters can be employed to interpret and process network protocol messages, such as those used
in communication between different devices or systems.

AI and Natural Language Processing: Interpreters are used in natural language processing systems to parse and understand
human language, allowing for chatbots, language translation, sentiment analysis, etc.

Simulation and modeling: Interpreters can be used to interpret scripts or configurations that define simulation
parameters or behaviors in scientific or engineering simulations.

Financial and mathematical modeling: Systems that interpret and execute financial models, perform risk analysis, or
simulate market behaviors might utilize the Interpreter pattern to process and evaluate these models.

java.util.Pattern
java.text.Normalizer
All subclasses of java.text.Format
All subclasses of javax.el.ELResolver

The Interpreter pattern is widely used in compilers implemented with object-
oriented languages, as the Smalltalk compilers are. SPECTalk uses the pattern to
interpret descriptions of input file formats [Sza92]. The QOCA constraint-solving
toolkit uses it to evaluate constraints [HHMV92].
Considered in its most general form (i.e., an operation distributed over a class
hierarchy based on the Composite pattern), nearly every use of the Composite
pattern will also contain the Interpreter pattern. But the Interpreter pattern should
be reserved for those cases in which you want to think of the class hierarchy as
defining a language.

### Categorization

Purpose:  **Behavioral**  
Scope:    **Class**   
Mechanisms: **Inheritance**

Behavioral patterns are concerned with algorithms and the assignment of responsibilities between objects.
Behavioral patterns describe not just patterns of objects or classes
but also the patterns of communication between them.

Behavioral class patterns use inheritance to distribute behavior between classes. This chapter includes two such
patterns.

Behavioral object patterns use object composition rather than inheritance.
Some describe how a group of peer objects cooperate to perform a task that no single object can carry out by itself.
An important issue here is how peer objects know about each other.
Peers could maintain explicit references to each other, but that would increase their coupling.
Some patterns provide indirection to allow loose coupling
mediator, chain of responsibility, observer
Other behavioral object patterns are concerned with encapsulating behavior in an object and delegating requests to it.
strategy, command, state, visitor, iterator

### Aspects that can vary

- Grammar and interpretation of a language.

### Solution to causes of redesign

None (known)

### Consequences

| Advantages                                         | Disadvantages                       |
|----------------------------------------------------|-------------------------------------|
| :heavy_check_mark: **Short description.** <br> ... | :x: **Short description.** <br> ... |

The Interpreter pattern has the following benefits and liabilities:

1. It's easy to change and extend the grammar. Because the pattern uses classes
   to represent grammar rules, you can use inheritance to change or extend
   the grammar. Existing expressions can be modified incrementally, and new
   expressions can be defined as variations on old ones
2. Implementing the grammar is easy, too.Classes defining nodes in the abstract
   syntax tree have similar implementations.These classes are easy to write, and
   often their generation can be automated with a compiler or parser generator.
3. Complex grammars arehard to maintain. The Interpreter pattern defines at least
   one class for every rule in the grammar (grammar rules defined using BNF
   may require multiple classes). Hence grammars containing many rules can
   be hard to manage and maintain. Other design patterns can be applied to
   mitigate the problem (see Implementation). But when the grammar is very
   complex, other techniques such as parser or compiler generators are more
   appropriate.
4. Adding new ways to interpret expressions. The Interpreter pattern makes it
   easier to evaluate an expression in a new way.For example, you can support
   pretty printing or type-checking an expression by defining a new operation
   on the expression classes. If you keep creating new ways of interpreting an
   expression, then consider using the Visitor (331) pattern to avoid changing
   the grammar classes

### Relations with Other Patterns

_Distinction from other patterns:_
_Combination with other patterns:_
- ...

Composite (163):The abstract syntax tree is an instance of the Composite pattern.
Flyweight (195)shows how to share terminal symbols within the abstractsyntax
tree.
Iterator (257): The interpreter can use an Iterator to traverse the structure.
Visitor (331) can be used to maintain the behavior in each node in the abstract
syntax tree in one class

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The pattern can be implemented by ...**

(recognizable by behavioral methods returning a structurally different instance/type of the given instance/type; note
that parsing/formatting is not part of the pattern, determining the pattern and how to apply it is)

### Structure

```mermaid
classDiagram
    class AbstractExpression {
        <<abstract>>
        - expression: AbstractExpression
        + interpret(context: Context): void
    }

    class TerminalExpression {
        + interpret(context: Context): void
    }

    class NonTerminalExpression {
        + interpret(context: Context): void
    }

    class Context {
        - input: string
        - output: any
    }

    Context <-- AbstractExpression: uses
    AbstractExpression <|-- TerminalExpression: extends
    AbstractExpression <|-- NonTerminalExpression: extends
    AbstractExpression o-- AbstractExpression: aggregated by
```

### Variations

_Variation name:_

- **VariationA**: ...
    - :heavy_check_mark: ...
    - :x: ...
- **VariationB**: ...
    - :heavy_check_mark: ...
    - :x: ...

The Interpreter and Composite (163) patterns share many implementation issues.
The following issues are specific to Interpreter:

1. Creating the abstract syntax tree. The Interpreter pattern doesn't explain how to
   create an abstract syntax tree. In other words, it doesn't address parsing. The
   abstract syntax tree can be created by a table-driven parser, by a hand-crafted
   (usually recursive descent) parser, or directly by the client.
2. Defining the Interpret operation. You don't have to define the Interpret oper-
   ation in the expression classes. If it's common to create a new interpreter,
   then it's better to use the Visitor (331) pattern to put Interpret in a separate
   "visitor" object. For example, a grammar for a programming language will
   have many operations on abstract syntax trees, such as as type-checking, op-
   timization, code generation, and so on. It will be more likely to use a visitor
   to avoid defining these operations on every grammar class.
3. Sharing terminal symbols with the Flyweight pattern. Grammars whose sentences
   contain many occurrences of a terminal symbol might benefit from sharing
   a single copy of that symbol. Grammars for computer programs are good
   examples—each program variable will appear in many places throughout the
   code. In the Motivation example, a sentence can have the terminal symbol
   dog (modeled by the LiteralExpression class) appearing many times.
   Terminal nodes generally don't store information about their position in the
   abstract syntax tree. Parent nodes pass them whatever context they need
   during interpretation. Hence there is a distinction between shared (intrinsic)
   state and passed-in (extrinsic) state, and the Flyweight (195) pattern applies. For example, each instance of
   LiteralExpression for dog receives a context
   containing the substring matched so far. And every such LiteralExpression
   does the same thing in its Interpret operation—it checks whether the next
   part of the input contains a dog—no matter where the instance appears in
   the tree.

### Implementation

In the example we apply the interpreter pattern to a ... system.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Interpreter](https://refactoring.guru/design-patterns/interpreter)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [DerekBanas: Interpreter Design Pattern](https://youtu.be/6CVymSJQuJE?si=dSbveW-H3IwED3d-)
<br>
<br>
